
package blog.model.dao;

import blog.model.entity.Comment;
import java.util.List;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;


@Repository
@Transactional(value = "transactionManager", propagation= Propagation.REQUIRED)
public class CommentDAOImpl implements CommentDAO{

    @Autowired
    SessionFactory sessionFactory;
    
    public void save(Comment comment) {
        sessionFactory.getCurrentSession().saveOrUpdate(comment);
    }

    public List<Comment> list() {
        return sessionFactory.getCurrentSession().createCriteria(Comment.class).list();
    }

    public Comment findById(long idComment) {
        return (Comment) sessionFactory.getCurrentSession().get(Comment.class, idComment);
    }
    
}
