
package blog.model.entity;

import java.util.Date;
import javax.persistence.*;

@Entity
@Table(name = "comments")
public class Comment {
    private long idComment;
    private String message;
    private Date created = null;
    private User user;
    private Post post;
    
    
    @Id @GeneratedValue
    @Column(name="idComment", insertable = false, updatable = false)
    public long getIdComment(){
        return idComment;
    }
    
    public void setIdComment(long idComment){
        this.idComment = idComment;
    }
    
    @Column(name="message")
    public String getMessage(){
        return message;
    }
    
    public void setMessage(String message){
        this.message = message;
    }
    @ManyToOne
    @JoinColumn(name = "idUser")
    public User getUser(){
        return user;
    }
    
    public void setUser(User user){
        this.user = user;
    }
    @ManyToOne
    @JoinColumn(name = "idPost")
    public Post getPost(){
        return post;
    }
    
    public void setPost(Post post){
        this.post = post;
    }
    
}
