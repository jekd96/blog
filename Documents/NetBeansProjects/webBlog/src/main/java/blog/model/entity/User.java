
package blog.model.entity;

import java.util.Date;
import javax.persistence.*;

@Entity
@Table(name = "users")
public class User {
    private long idUser;
    private String email;
    private String password;
    private String nameUser;
    private Date dateRegistration = null;
    
    @Id @GeneratedValue
    @Column(name="idUser", insertable = false, updatable = false)
    public long getIdUser(){
        return idUser;
    }
    
    public void setIdUser(long idUser){
        this.idUser = idUser;
    }
    
    @Column(name="email")
    public String getEmail(){
        return email;
    }
    
    public void setEmail(String email){
        this.email = email;
    }
    
    @Column(name="password")
    public String getPassword(){
        return password;
    }
    
    public void setPassword(String password){
        this.password = password;
    }
    
    @Column(name = "nameUser")
    public String getNameUser(){
        return nameUser;
    }
    
    public void setNameUser(String nameUser){
        this.nameUser = nameUser;
    }
 
    @Column(name = "dateRegistration")
    @Temporal(javax.persistence.TemporalType.DATE)
    public Date getDateRegistration(){
        return dateRegistration;
    }
    
    public void setDateRegistration(Date dateRegistration){
        this.dateRegistration = dateRegistration;
    }
}
