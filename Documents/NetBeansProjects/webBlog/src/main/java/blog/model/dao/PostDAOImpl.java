
package blog.model.dao;

import blog.model.entity.Post;
import java.util.List;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;


@Repository
@Transactional(value = "transactionManager", propagation= Propagation.REQUIRED)
public class PostDAOImpl implements PostDAO{
    
    @Autowired
    SessionFactory sessionFactory;
    
    public void save(Post post) {
        sessionFactory.getCurrentSession().saveOrUpdate(post);
    }

    public List<Post> list() {
        return sessionFactory.getCurrentSession().createCriteria(Post.class).list();
    }

    public Post findById(long idUser) {
        return (Post) sessionFactory.getCurrentSession().get(Post.class, idUser);
    }
   
}
