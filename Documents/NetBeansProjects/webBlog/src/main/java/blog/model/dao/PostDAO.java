
package blog.model.dao;

import blog.model.entity.Post;
import java.util.List;

public interface PostDAO {
    public void save(Post post);
    public List<Post> list();
    public Post findById(long idUser);
}
