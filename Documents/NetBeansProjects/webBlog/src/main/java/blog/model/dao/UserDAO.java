
package blog.model.dao;

import blog.model.entity.User;
import java.util.List;

public interface UserDAO {
    public void save(User user);
    public List<User> list();
    public User findById(long idUser);
}
