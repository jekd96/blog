
package blog.model.dao;

import blog.model.entity.User;
import java.util.List;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;


@Repository
@Transactional(value = "transactionManager", propagation= Propagation.REQUIRED)
public class UserDAOImpl implements UserDAO{
    
    @Autowired
    SessionFactory sessionFactory;
    
    public void save(User user) {
        sessionFactory.getCurrentSession().saveOrUpdate(user);
    }

    public List<User> list() {
        return sessionFactory.getCurrentSession().createCriteria(User.class).list();
    }

    public User findById(long idUser) {
        return (User) sessionFactory.getCurrentSession().get(User.class, idUser);
    }
    
}
