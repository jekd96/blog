
package blog.model.entity;

import java.util.Date;
import javax.persistence.*;

@Entity
@Table(name = "posts")
public class Post {
    private long idPost;
    private String title;
    private String postContent;
    private Date created = null;
    private User user;
    
    @Id @GeneratedValue
    @Column(name="idPost", insertable = false, updatable = false)
    public long getIdPost(){
        return idPost;
    }
    
    public void setIdPost(long idPost){
        this.idPost = idPost;
    }
    
    @Column(name="title")
    public String getTitle(){
        return title;
    }
    
    public void setTitle(String title){
        this.title = title;
    }
    
    @Column(name="postContent")
    public String getPostContent(){
        return postContent;
    }
    
    public void setPostContent(String postContent){
        this.postContent = postContent;
    }
    
    @Column(name="created")
    @Temporal(javax.persistence.TemporalType.DATE)
    public Date getCreated(){
        return created;
    }
    
    public void setCreated(Date created){
        this.created = created;
    }
    
    @ManyToOne
    @JoinColumn(name = "idUser")
    public User getUser(){
        return user;
    }
    
    public void setUser(User user){
        this.user = user;
    }
}
