
package blog.model.dao;

import blog.model.entity.Comment;
import java.util.List;


public interface CommentDAO {
    public void save(Comment comment);
    public List<Comment> list();
    public Comment findById(long idComment);
}
