CREATE TABLE posts (
    idPost bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    title varchar(255) COLLATE utf8_bin NOT NULL,
    postContent text COLLATE utf8_bin NOT NULL,
    created date DEFAULT NULL,
    idUser bigint(20) NOT NULL
);
CREATE TABLE users (
    idUser bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    email varchar(255) COLLATE utf8_bin NOT NULL,
    password varchar(255) COLLATE utf8_bin NOT NULL,
    nameUser varchar(255) COLLATE utf8_bin NOT NULL,
    dateRegistration date DEFAULT NULL
);
CREATE TABLE comments(
    idComment bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    message text COLLATE utf8_bin NOT NULL,
    created date DEFAULT NULL,
    idUser bigint(20) DEFAULT NULL,
    idPost bigint(20) NOT NULL
);
